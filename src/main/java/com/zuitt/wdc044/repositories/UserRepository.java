package com.zuitt.wdc044.repositories;

import com.zuitt.wdc044.models.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

// an interface contains behavior that a class implements
// an interface marked as @Repository contains methods for "database manipulation".
// by extending CrudRepository, PostRepository has inherited its pre-defined methods for creating, retrieving, updating, and deleting records
@Repository
public interface UserRepository extends CrudRepository<User, Object> {
    // A custom method for finding a user by their username

    User findByUsername(String username);
}