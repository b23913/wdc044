package com.zuitt.wdc044.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.Set;
import javax.persistence.*;
// It mark this Java object as a representation of a database table via @Entity
@Entity
// designate the table name
@Table(name = "users")
public class User {
    // indicates that this property is the primary key.
    @Id
    // auto-increment
    @GeneratedValue
    private Long id;

    // class properties that represent a table column
    @Column
    private String username;

    @Column
    private String password;

    // Represents the one side of the relationship
    @OneToMany(mappedBy = "user")
    // infinite recursion
    @JsonIgnore
    private Set<Post> posts;

    // default constructor, this is needed when retrieving posts
    public User(){}

    public User (String username, String password){
        this.username = username;
        this.password = password;
    }

    //getter & setter
    public Long getId(){
        return id;
    }

    public String getUsername(){
        return username;
    }
    public void setUsername(String username){
        this.username = username;
    }
    public String getPassword(){
        return password;
    }

    public void setPassword(String password){
        this.password = password;
    }

    // getting the posts
    public Set<Post> getPosts(){
        return posts;
    }

}
